Keycloak Apps Themes
====================

Themes dans 'src/main/resources/theme'

    - 1 themes par dossier
    - le nom du thème est le nom du dossier
    - pour chaque thème, il existe plusieurs étapes du workflow keycloak (login, account, admin, welcome, ....)
    - liste des thèmes 
        - addresses-eole
        - logo-eole
        - pad-eole
        - sunrise-eole
    
Catalogue des thèmes  dans 'src/main/resources/META-INF'

    'keycloak-themes.json' (format utilisé par keycloak)
    
Run :

    ./mvnw package

packaging dans: ( à injecter dans eole3_keycloak/libs )

     'target/apps-themes-1.1-eole3.jar'


Test en local :

    bash run-local.sh
    
    fait :
    - build jar
    - créer docker image 'apps-themes:latest' from eole3_keycloak
    - démarre image apps-themes (donc, avec nouveau Jar)
    - Attend démarrage
    - Intialise keycloak avec création gilles, tests
    - ouvre firefox http://localhost:8080/auth/realms/master/protocol/openid-connect/auth?client_id=security-admin-console
    tada... : le théme apparait
