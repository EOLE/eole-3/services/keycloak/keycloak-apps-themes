FROM hub.eole.education/test/eole3-keycloak:22.0.5-eole3.1
RUN rm -rf /opt/keycloak/themes/* || true
COPY src/main/resources/theme/apps-theme/ /opt/keycloak/themes/
